# For Declaration
from sqlalchemy import Column, Integer, UnicodeText, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, desc

# For Session
from sqlalchemy.orm import sessionmaker, scoped_session
from contextlib import contextmanager
from sqlite3 import OperationalError
import time
import random

from consts import PATH_TO_DB
from utils import ugly_try

Base = declarative_base()


class Like(Base):
    __tablename__ = 'likes'
    id = Column(Integer, primary_key=True)
    post_id = Column(UnicodeText)
    post_creation_date = Column(DateTime)
    poster_id = Column(UnicodeText)
    liker_id = Column(UnicodeText)


def create_db():
    # Create an engine that stores data in the local directory's likes.db file
    # engine = create_engine('sqlite:///' + PATH_TO_DB)

    # Create all tables in the engine. This is equivalent to "Create Table" statements in raw SQL.
    Base.metadata.create_all(engine)


# Bind the engine to the metadata of the Base class so that the
# declaratives can be accessed through a DBSession instance
engine = create_engine('mysql://ronen:ronen@localhost/ronen', pool_size=0)
Base.metadata.bind = engine
DBSession = scoped_session(sessionmaker(bind=engine))


@contextmanager
def session_scope():
    """Provide a transactional scope around a series of operations."""
    session = DBSession()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


@ugly_try
def insert_likes_to_table(likes):
        with session_scope() as session:
            session.add_all(likes)

@ugly_try
def get_all_likes():
    session = DBSession()
    return session.query(Like).all()


@ugly_try
def get_likes_by_poster_id(poster_id):
    session = DBSession()
    return session.query(Like).filter(Like.poster_id == poster_id).all()


@ugly_try
def get_likes_by_post_id(post_id):
    session = DBSession()
    return session.query(Like).filter(Like.post_id == post_id).all()


@ugly_try
def delete_likes_by_post_id(post_id):
    with session_scope() as session:
        return session.query(Like).filter(Like.post_id == post_id).delete()


@ugly_try
def get_highest_id():
    session = DBSession()
    return session.query(Like).order_by(desc(Like.id)).first().id


# Not really needed, since id is the primary key, you don't need to define it explicitly in the Like constructor.
# Left here for possible further use...
def generate_id():
    num = get_highest_id() + 1
    while True:
        yield num
        num += 1

likes_uuid = generate_id()
