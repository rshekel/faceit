from flask import Flask, render_template
# from jinja2 import 

app = Flask(__name__)


class Person(object):
    def __init__(self, first_name, last_name, username, title):
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.title = title


def get_persons():
    # TODO: create a table in DB with current people we are tracking, and return a draw from that table.
    persons = [
        Person("Benjamin", "Netanyahu", "Netanyahu", "Prime Minister"),
        Person("Yair", "Lapid", "YairLapid", "Funny Person"),
        Person("Shimon", "Peres", "ShimonPeresInt", "Old man"),
    ]
    return persons


@app.route("/")
def root():
    return render_template("mainpage.html", persons=get_persons())


@app.route("/about")
def about():
    return render_template("b.html", persons=get_persons())


def main():
    app.run(debug=True)


if __name__ == "__main__":
    main()
