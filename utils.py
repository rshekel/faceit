import requests
import threading
import time
import random
import sys
import functools
from sqlite3 import OperationalError


class Obj(object):
    """
        A helper class that turns a dict to a cool fun object with auto-completion and stuff...
    """
    def __init__(self, d):
        self._orig = d
        for a, b in d.items():
            if isinstance(b, (list, tuple)):
               setattr(self, a, [Obj(x) if isinstance(x, dict) else x for x in b])
            else:
               setattr(self, a, Obj(b) if isinstance(b, dict) else b)

    def __repr__(self):
        try:
            if hasattr(self, 'is_hidden'):
                return "<Post by: %s>" % str(self._orig['from']['name'].decode("utf-8"))
            elif not hasattr(self, 'message'):
                return "<Like by: %s>" % str(self.name.decode("utf-8"))
            else:
                return "<Comment by: %s>" % str(self._orig['from']['name'].decode("utf-8"))
        except:
            return "Some Bad Unicode Stuff..."


def generate_more(starting_page, pages=5):
    """
        A generic function that crawls to get more of the same thing (page after page of posts / likes / comments / shares etc...)
        @params:
        starting_page - object that we will start crawling from (type: Obj)
        pages - amount of pages to crawl over... 0 for infinity.
    """
    res = []
    cur_page = starting_page

    page = 0
    while True:
        page += 1
        try:
            print "Retrieving..."
            res += cur_page.data

            # Attempt to make a request to the next page of data, if it exists.
            cur_page = Obj(requests.get(cur_page.paging.next).json())

        except AttributeError:
            # When there are no more pages (['paging']['next']), break from the loop
            break
        except:
                print "Some error occurred during request of next page..., probably a timeout..."
                print sys.exc_info()

        # If pages == 0, it will continue till end
        if page == pages:
            break

    return res


def async(f):
    def wrapper(*args, **kwargs):
        # print "in decorator"
        q = threading.Thread(target=f, args=args)
        q.start()
        return q
    return wrapper


# Some ugly stuff because i didn't quite understand yet how to work multithreaded without getting Lock issues.
def ugly_try(f):
    def wrapper(*args, **kwargs):
        count = 1
        while True:
            try:
                ret = f(*args, **kwargs)
                return ret
            # Usually sqlite3.Operational Error, but also other thread safety related issues...
            except:
                print "Problem connecting to db... Trying again for the %s time... " % str(count)
                count += 1
                if count > 5:
                    print "quitting trying to connect to db after 5 times..."
                    break
                time.sleep(random.randint(1, 30))
    return wrapper

