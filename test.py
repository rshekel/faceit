import facebook
from faceit import *
from utils import async, ugly_try
from consts import PATH_TO_DB
from orm import create_db
import os
import time

# 34 politicians
POLITICIANS = ["Netanyahu", "YairLapid", "ShimonPeresInt", "NaftaliBennett", "miri.regev.il",
                    "DanonDanny", "AvigdorLiberman", "ShellyYachimovich", "ReuvenRivlin", "ayelet.benshaul.shaked",
                    "nir.barkat", "MFeiglin", "tzipilivni", "IsaacHerzog", "Moshekahalon", "zehavagalon", "katzisraellikud",
                    "Meretz", "tamarzandberg", "TzipiHotovely", "MichaeliMerav", "dovhanin",
                    "173196886046831", "DeryArye", "gilad.erdan", "GideonSaarLikud", "Peretz.Amir", "erelmargalit",
                    "Tzachi.Hanegbi", "OfirAkunis", "MichaelsonSonLion", "YuliEdelstein", "eitan.cabel"]

POL2 = [
                    "AmbassadorOren", "hamachanehazioni", "EliyahuYishai", "itzik.shmuli", "michal.rozin",
                    "baityehudi.p", "MKIlanGilon", "MK.Gila.Gamliel", "orly.levyabkasis", "aliza.lavie",
                    "yariv.levin.9", "Officialbaruchmarzel", "miki.rosenthal", "kisch.yoav", "SternElazar",
                    "EllibenDahan", "karine.elharrar", "YoavGallant", "uriariel.co.il", "mtzd.yoni.chetboun",
                    "Shuli.Mualem.Refaeli.Bayit.Yehudi", "barlev.omer", "mottiyogev", "orenhazanlikud",
                    "OferYeshAtid", "GermanYeshAtid", "Elkin.Zeev", "YossiYonah", "NachmanShai",
                    "meircoh", "rachel.azaria", "Bezazelsmotrich", "zoharm7", "166156570202888", "RabbiGiladKariv",
                    "yoel.razvozov", "350522538451081", "AviDichter1", "SwidRevital", "MKHasson",
                    "SvetlovaKsenia", "navaboker", "Yaakov.Perry"]


def main():

    # From the sqlite days...
    # if not os.path.isfile(PATH_TO_DB):
    #    print "Creating DB..."
    #    create_db()

    # You'll need an access token here to do anything.  You can get a temporary one
    # here: https://developers.facebook.com/tools/explorer/
    access_token = 'CAACEdEose0cBAJD5bZCQwi7ZCpk6DMFxtCO6ueMuWsYZALys5E2rbLyqf2W2KUvCh6TTuMyf29GNaAor67aOfLeYKyy3q3rHaoDfP40ZCF8ZAEsEwseqSTmkUrGuUQ6H5nG0IXHKsDkxV4gCjatjEB7GBY8UPYP2SLMjrZB5DgukWHOIfZCbeQNkZCte6zXNjwm7XcTR4UZA9exH9ySVQ87Gq'
    graph = facebook.GraphAPI(access_token)

    for name in POLITICIANS:
        print "retrieving posts for: ", name
        # Let the mysql server to start them one by one...
        time.sleep(1)
        do_get_likes(graph, name)


@ugly_try
@async
def do_get_likes(graph, name):
    p = Person(graph, name)
    posts = p.get_posts(force=True, pages=5)
    for post in posts:
        _get_likes_from_post_async(post)


def _get_likes_from_post_async(post):
    # pages is 0 so it will get all the likes
    print "Retrieving likes from post: ", post._id
    return post.get_likes(force=True, pages=0)


if __name__ == "__main__":
    main()
