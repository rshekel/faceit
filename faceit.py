import orm
from utils import generate_more, Obj
from consts import date_time_facebook_format
import datetime


class Person(object):
    def __init__(self, graph,  name):
        """
        Insert name for public figure. Else - enter ID. 
        """
        self._graph = graph
        self._object = self._graph.get_object(name)
        self.name = self._object['name']
        self.id = self._object['id']
        self._posts = []

    def get_posts(self, force=False, pages=5):
        if self._posts == [] or force is True:
            self._posts = []
            posts = Obj(self._graph.get_connections(self.id, 'posts'))
            
            self._posts = [Post(post) for post in generate_more(posts, pages=pages)]
            
            return self._posts

        else:
            return self._posts

        
class Post(object):
    def __init__(self, post):
        """
            Post object, constructed by an Obj object, that was constructed by a dictionary of a post
        """
        self._original = post
        self._id = self._original.id
        self.poster = self._original._orig['from']
        if hasattr(post, "message"):
            self.message = self._original.message
        self.update_time = datetime.datetime.strptime(self._original.updated_time, date_time_facebook_format)
        self.created_time = datetime.datetime.strptime(self._original.created_time, date_time_facebook_format)

        self._comments = []
        self._shares = []

    def __repr__(self):
        return self._original.__repr__()
    
    def get_comments(self, force=False, pages=5):
        if self._comments == [] or force is True:
            self._comments = []
            comments = self._original.comments
            
            self._comments = generate_more(comments, pages=pages)
            
            return self._comments

        else:
            return self._comments

    def get_likes(self, force=False, pages=10):
        """
        :param force: force to refetch from facebook
        :param pages: amount of pages to crawl. 0 for infinity.
        :return:
        """
        if orm.get_likes_by_post_id(self._id) == [] or force is True:
            if force:
                # So we won't have duplicates...
                orm.delete_likes_by_post_id(self._id)
            likes = self._original.likes
            likes = generate_more(likes, pages=pages)
            orm_likes = []
            for like in likes:
                orm_like = orm.Like(post_id=self._id,
                                    post_creation_date=self.created_time,
                                    poster_id=self.poster['id'],
                                    liker_id=like.id)
                orm_likes.append(orm_like)
            orm.insert_likes_to_table(orm_likes)
            return orm.get_likes_by_post_id(self._id)

        else:
            return orm.get_likes_by_post_id(self._id)
    
    def get_shares(self):
        return self._original.shares.count
    

